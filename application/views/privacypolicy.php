<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo $this->ConfigModel->getMetaDescription(); ?>">
        <meta name="keyword" content="<?php echo $this->ConfigModel->getMetaKeyword(); ?>">
        <title>The Stage</title>
        <link rel="icon" href="<?php echo $this->ConfigModel->getFavicon(); ?>">
        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <!-- Theme -->
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
        <!-- Font -->
        <link href="<?php echo base_url('assets/fonts/DB_Helvethaica_X/dbhelvethaicax.css'); ?>" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src = "https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
              <![endif]-->

        <?php echo $this->ConfigModel->getGoogleAnalytic(); ?>
    </head>
    <body>
        <?php echo $this->ConfigModel->getGoogleRemarketing(); ?>
        <header>
            <center><?php echo anchor(base_url(), img(array('src' => 'assets/images/logo.png', 'class' => 'img-logo',))); ?></center>
        </header>
        <div role="main" class="main-content">
            <div class="container privacy-container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <h1>Privacy  Policy</h1>

                        <h2>นโยบายคุ้มครองข้อมูลส่วนบุคคล</h2>
                        <p>
                            บริษัท เรียลแอสเสท ดีเวลลอปเมนท์ จำกัด ได้จัดทำเว็บไซต์ขึ้นมาเพื่อนำเสนอข้อมูลโครงการ The Stage ซึ่งคุณสามารถเข้าไปเยี่ยมชมเว็บไซต์ของเราได้ตามต้องการ โดยไม่จำเป็นต้องให้ข้อมูลของตัวคุณเอง ทั้งนี้ ในบางกรณี เราอาจจำเป็นต้องใช้ข้อมูลของคุณเพื่อให้บริการในสิ่งที่คุณต้องการ
                        </p>
                        <h2>การรวบรวมข้อมูลส่วนบุคคล</h2>
                        <p>
                            เราจะทำการสอบถามข้อมูลส่วนตัวของคุณ เพื่อความสะดวกในการติดต่อกลับ ซึ่งในกรณีนี้ คุณจำเป็นจะต้องลงทะเบียนก่อน จึงจะสามารถรับข่าวสาร ข้อมูล หรือรับสิทธิพิเศษ พร้อมลงทะเบียนเข้ารับการอบรม หรือจัดกิจกรรมต่างๆ ซึ่งข้อมูลส่วนบุคคลที่รวบรวมโดยบริษัท เรียลแอสเสท ดีเวลลอปเมนท์ จำกัดนั้น จะมีทั้งอีเมล์ ที่อยู่ ชื่อ เบอร์โทรติดต่อ ซึ่งอาจจะรวมอยู่ในข้อมูลประเภทอื่นๆด้วยเมื่อคุณต้องการรับบริการจากเรา 
                        </p>
                        <p>
                            นอกจากนี้ บริษัทฯยังเก็บรวบรวมข้อมูลของระบบซอฟแวร์และฮาร์ดแวร์จากเครื่องคอมพิวเตอร์ของคุณ เพื่อบันทึก IP address, browser type, operating system, domain name, access times และ referring Web site Addresses ซึ่งข้อมูลเหล่านี้จะเป็นประโยชน์ต่อการให้บริการของเรา เพื่อคงไว้ซึ่งคุณภาพระดับสากล
                        </p>
                        <h2>ระบบควบคุมข้อมูลส่วนบุคคล</h2>
                        <p>
                            หลังจากได้ลงทะเบียนเรียบร้อยแล้ว http://www.thestagecondo.com/ จะเก็บข้อมูลของคุณไว้เป็นความลับ เว้นแต่ในกรณีที่ต้องมีการตรวจสอบทางกฎหมาย หรือเท่าที่จำเป็นเพื่อปกป้องคุ้มครองสิทธิหรือทรัพย์สินของทางบริษัทฯ นอกจากนี้การลงทะเบียน ยังช่วยให้เรารับรู้ถึงความต้องการที่คุณมีต่อเรา เพื่อให้เราได้ปรับปรุงการให้บริการและการสื่อสารผ่านทางเว็บไซต์ให้ดียิ่งขึ้น
                        </p>
                        <h2>การใช้ระบบ Cookies เพื่อช่วยในการจดจำและบันทึกข้อมูล</h2>
                        <p>
                            เมื่อใดก็ตามที่มีคนเข้ามาเยี่ยมชมเว็บไซต์ ระบบ Cookies จะถูกป้อนเข้าในหน่วยความจำของคอมพิวเตอร์ลูกค้า (ในกรณีที่เครื่องสามารถรับ Cookies ได้) และจะทำการบันทึกไว้ว่ามีการเข้าเยี่ยมชมเว็บไซต์ ด้วยระบบนี้ เราจะสามารถบันทึกข้อมูลสถิติการเข้าชมเว็บไซต์ ดังที่กล่าวมา</p>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container footer-container">
                <div class="row">
                    <div class="col-md-4">
                        <?php echo anchor('https://www.facebook.com/RealAssetDevelopment', img(array('src' => 'assets/images/footer/icon-fb.png',)), array('target'=>'_blank','class'=>'pull-left')); ?>
                        <?php echo anchor('http://www.youtube.com/user/RealAssetProperty', img(array('src' => 'assets/images/footer/icon-youtube.png',)), array('target'=>'_blank','class'=>'pull-left')); ?>
                        <a href="tel:088-004-5551" class="pull-left">
                        <?php echo img(array('src' => 'assets/images/footer/tel.png','class'=>'img-responsive','style'=>'width:auto; height:auto;')); ?>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="pull-right">
                            <?php echo anchor(base_url(), img(array('src' => 'assets/images/footer/footer-logo.png',))); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9 copyright">
                        <label>www.realasset.co.th  © 2014 Real Asset Development Co.,Ltd. All Right Reserved. <a href="tel:02-652-8852">tel:02-652-8852</a></label>
                    </div>
                    <div class="col-md-3 privacy">
                        <label><a href="<?php echo site_url('privacypolicy'); ?>">Privacy Policy</a></label>
                    </div>
                </div>
            </div>
        </footer>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    </body>
</html>