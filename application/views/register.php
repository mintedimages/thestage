<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo $this->ConfigModel->getMetaDescription(); ?>">
        <meta name="keyword" content="<?php echo $this->ConfigModel->getMetaKeyword(); ?>">
        <title>The Stage</title>
        <link rel="icon" href="<?php echo $this->ConfigModel->getFavicon(); ?>">
        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <!-- Theme -->
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
        <!-- Font -->
        <link href="<?php echo base_url('assets/fonts/DB_Helvethaica_X/dbhelvethaicax.css'); ?>" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src = "https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

        <?php echo $this->ConfigModel->getGoogleAnalytic(); ?>

        <style>
            .modal-backdrop.in {
                filter: alpha(opacity=70);
                opacity: .7;
            }

            @media (min-width: 768px) {
                .modal-dialog {
                    width: 800px;
                }
            }
            @media (min-width: 992px) {
                .modal-dialog {
                    width: 1000px;
                }
            }
        </style>
    </head>
    <body>
        <?php echo $this->ConfigModel->getGoogleRemarketing(); ?>
        <header>
            <center><?php echo anchor(base_url(), img(array('src' => 'assets/images/logo.png', 'class' => 'img-logo',))); ?></center>
        </header>
        <div role="main" class="main-content">
            <div class="container register-container">
                <div class="row">
                    <div class="col-md-5">
                        <?php echo img(array('src' => 'assets/images/thestrage.gif', 'class' => 'img-responsive')); ?>
                    </div>
                    <div class="col-md-7">
                        <?php
$attributes = array('class' => 'register-form', 'id' => 'register-form');
echo form_open('register/send/' . $this->FatherModel->getDefaultShop()->shop_id . '/0', $attributes);
                        ?>
                        <div class="input-container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputName">Name</label>
                                        <input type="text" class="form-control" id="inputName" placeholder="Enter Name" name="txtName"  data-validation="required" data-validation-error-msg="Please input require." tabindex="1">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputLastName">Last Name</label>
                                        <input type="text" class="form-control" id="inputLastName" placeholder="Enter Last Name" name="txtLastName" data-validation="required" data-validation-error-msg="Please input require." tabindex="2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputMobilePhone">Mobile Phone</label>
                                        <input type="text" class="form-control" id="inputMobilePhone" placeholder="Enter Mobile Phone" name="txtMobilePhone" data-validation="length number" data-validation-length="10-10" data-validation-error-msg="Please input valid mobile phone." tabindex="3">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail">Email</label>
                                        <input type="text" class="form-control" id="inputEmail" placeholder="Enter Email" name="txtEmail" data-validation="email" data-validation-error-msg="Please input valid email." tabindex="4">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputRoomType">Room Type</label>
                                        <select class="form-control" id="inputRoomType" name="txtRoomType"  tabindex="5">
                                            <option value="1 ห้องนอน">1 ห้องนอน</option>
                                            <option value="2 ห้องนอน">2 ห้องนอน</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputBudget">Budget</label>
                                        <select class="form-control" id="inputBudget" name="txtBudget" tabindex="6">
                                            <option value="2 – 2.5 ล้านบาท">2 – 2.5 ล้านบาท</option>
                                            <option value="2.5 – 3 ล้านบาท">2.5 – 3 ล้านบาท</option>
                                            <option value="3.5 – 4 ล้านบาท">3.5 – 4 ล้านบาท</option>
                                            <option value="4.5 – 5 ล้านบาท">4.5 – 5 ล้านบาท</option>
                                            <option value="5 ล้านขึ้นไป">5 ล้านขึ้นไป</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="inputMessage">Message</label>
                                        <textarea class="form-control" id="inputMessage" name="txtMessage" rows="3" tabindex="7"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo img(array('src' => 'assets/images/corner_01.png', 'class' => 'form-corner')); ?>
                        <button type="submit" class="btnSubmit"><?php echo img(array('src' => 'assets/images/btn-submit.png')); ?></button>
                        
                        <a href="#" class="pull-left" style="display: block; width: 62px; margin: 5px 0;" data-toggle="modal" data-target="#myModal">
                            <img src="<?php echo base_url('assets/images/map.png'); ?>" alt="" style="width: 100%;">
                        </a>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute; top: 0; right: 0; background-color: #fff; width: 34px; height: 34px; border-radius: 22px; color: #000; font-weight: bold; font-size: 32px; opacity: 1;">&times;</button>
                                        <img src="<?php echo base_url('assets/images/map.jpg'); ?>" alt="" style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container footer-container">
                <div class="row">
                    <div class="col-md-8">
                        <?php echo anchor('https://www.facebook.com/RealAssetDevelopment', img(array('src' => 'assets/images/footer/icon-fb.png',)), array('target'=>'_blank','class'=>'pull-left')); ?>
                        <?php echo anchor('http://www.youtube.com/user/RealAssetProperty', img(array('src' => 'assets/images/footer/icon-youtube.png',)), array('target'=>'_blank','class'=>'pull-left')); ?>
                        <a href="tel:088-004-5551" class="pull-left">
                        <?php echo img(array('src' => 'assets/images/footer/tel.png','class'=>'img-responsive','style'=>'width:auto; height:auto;')); ?>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="pull-right">
                            <?php echo anchor(base_url(), img(array('src' => 'assets/images/footer/footer-logo.png',))); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9 copyright">
                        <label>www.realasset.co.th  © 2014 Real Asset Development Co.,Ltd. All Right Reserved. <a href="tel:02-652-8852">tel:02-652-8852</a></label>
                    </div>
                    <div class="col-md-3 privacy">
                        <label><a href="<?php echo site_url('privacypolicy');?>">Privacy Policy</a></label>
                    </div>
                </div>
            </div>
        </footer>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <!-- Form Validator -->
        <script src="<?php echo base_url('assets/js/plugins/formvalidator/jquery.form-validator.min.js'); ?>"></script>
        <script> $.validate();</script>
    </body>
</html>
