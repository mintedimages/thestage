<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo $this->ConfigModel->getMetaDescription(); ?>">
        <meta name="keyword" content="<?php echo $this->ConfigModel->getMetaKeyword(); ?>">
        <title>The Stage</title>
        <link rel="icon" href="<?php echo $this->ConfigModel->getFavicon(); ?>">
        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <!-- Theme -->
        <link href="<?php echo base_url('assets/css/web.css'); ?>" rel="stylesheet">
        <!-- Font -->
        <link href="<?php echo base_url('assets/fonts/DB_Helvethaica_X/dbhelvethaicax.css'); ?>" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src = "https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

        <?php echo $this->ConfigModel->getGoogleAnalytic(); ?>
    </head>
    <body class="home">
        <?php echo $this->ConfigModel->getGoogleRemarketing(); ?>
        <header>
            <div class="container header-container">
                <nav>
                    <ul class="clearfix">
                        <li>
                            <a href="<?php echo site_url();?>">
                                <img src="<?php echo base_url('assets/images/template/navigate/home.png');?>" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url();?>">
                                <img src="<?php echo base_url('assets/images/template/navigate/amenity.png');?>" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url();?>">
                                <img src="<?php echo base_url('assets/images/template/navigate/inspiration.png');?>" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url();?>">
                                <img src="<?php echo base_url('assets/images/template/navigate/gallery.png');?>" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url();?>">
                                <img src="<?php echo base_url('assets/images/template/navigate/location.png');?>" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url();?>">
                                <img src="<?php echo base_url('assets/images/template/navigate/news.png');?>" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url();?>">
                                <img src="<?php echo base_url('assets/images/template/navigate/contactus.png');?>" alt="">
                            </a>
                        </li>
                    </ul>
                </nav>
                <a href="<?php echo site_url();?>" class="logo">
                    <img src="<?php echo base_url('assets/images/template/logo_thestage.png');?>" alt="">
                </a>

            </div>
        </header>
        <div role="main" class="main-content">
            <div class="container">
                <?php echo $content;?>
            </div>
        </div>
        <footer>
            <div class="container footer-container">
                <div class="row">
                    <div class="col-md-8">
                        <?php echo anchor('https://www.facebook.com/RealAssetDevelopment', img(array('src' => 'assets/images/footer/icon-fb.png',)), array('target'=>'_blank','class'=>'pull-left')); ?>
                        <?php echo anchor('http://www.youtube.com/user/RealAssetProperty', img(array('src' => 'assets/images/footer/icon-youtube.png',)), array('target'=>'_blank','class'=>'pull-left')); ?>
                        <a href="tel:088-004-5551" class="pull-left">
                            <?php echo img(array('src' => 'assets/images/footer/tel.png','class'=>'img-responsive','style'=>'width:auto; height:auto;')); ?>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="pull-right">
                            <?php echo anchor(base_url(), img(array('src' => 'assets/images/footer/footer-logo.png',))); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9 copyright">
                        <label>www.realasset.co.th  © 2014 Real Asset Development Co.,Ltd. All Right Reserved. <a href="tel:02-652-8852">tel:02-652-8852</a></label>
                    </div>
                    <div class="col-md-3 privacy">
                        <label><a href="<?php echo site_url('privacypolicy');?>">Privacy Policy</a></label>
                    </div>
                </div>
            </div>
        </footer>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <!-- Form Validator -->
        <script src="<?php echo base_url('assets/js/plugins/formvalidator/jquery.form-validator.min.js'); ?>"></script>
        <script> $.validate();</script>
    </body>
</html>
