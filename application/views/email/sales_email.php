<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="background-color: #aeaeae;">
    <tbody>
        <tr>
            <td>
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tbody>
                        <tr>
                            <td style="border:1px dotted #D6d7d8; border-top:3px solid #0a4868; border-bottom: 3px solid #d6d7d8;background-color:#ee7202;padding: 30px;">
                                <p style="text-align:center;color: #000000;font-family: helvetica, arial, sans-serif;font-size: 24px;line-height: 1.2;padding-bottom: 20px;background-color: #ee7202;">
                                    <a href="<?php echo base_url(); ?>" target="_blank">
                                        <strong>
                                            <img src="<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/assets/images/logo.png" alt="<?php echo $this->ConfigModel->getWebsiteName($shop_id); ?>" />
                                        </strong>
                                    </a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border:1px dotted #D6d7d8; border-bottom: 3px solid #d6d7d8;background-color:#ffffff;padding: 30px;">
                                <p style="color: #456;font-family: helvetica, arial, sans-serif;font-size: 20px;line-height: 1.2;padding: 0;">
                                    <strong>ข้อมูลผู้สมัคร,</strong>
                                </p>
                                <p style="color: #456;font-family: helvetica, arial, sans-serif;font-size: 18px;line-height: 1.4;padding: 0;">
                                <div style="padding-bottom:5px;">
                                    <label style="width:150px;display:inline-block;">ชื่อ-นามสกุล</label>
                                    <span><?php echo ': ' . $txtName . ' ' . $txtLastName; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:150px;display:inline-block;">เบอร์โทร</label>
                                    <span><?php echo ': ' . $txtMobilePhone; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:150px;display:inline-block;">อีเมล</label>
                                    <span><?php echo ': ' . $txtEmail; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:150px;display:inline-block;">ความสนใจ</label>
                                    <span><?php echo ': ' . $txtRoomType; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:150px;display:inline-block;">Budget</label>
                                    <span><?php echo ': ' . $txtBudget; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:150px;display:inline-block;">ข้อความ</label>
                                    <span><?php echo ': ' . $txtMessage; ?></span>
                                </div>
                                <div style="padding-bottom:5px;">
                                    <label style="width:150px;display:inline-block;">รหัสลงทะเบียน</label>
                                    <span><?php echo ': ' . $register_code; ?></span>
                                </div>
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
