<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Register
 *
 * @author arsan
 */
class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('register');
    }

    public function send($shop_id = 0, $parent_id = 0) {
        $txtName = $_POST['txtName'];
        $txtLastName = $_POST['txtLastName'];
        $txtMobilePhone = $_POST['txtMobilePhone'];
        $txtEmail = $_POST['txtEmail'];
        $txtRoomType = $_POST['txtRoomType'];
        $txtBudget = $_POST['txtBudget'];
        $txtMessage = $_POST['txtMessage'];
        if (!$this->SpamModel->check_spam($txtName,true) && !$this->SpamModel->check_spam($txtLastName,true) && !$this->SpamModel->check_spam($txtMobilePhone,true) && !$this->SpamModel->check_spam($txtEmail,true) && !$this->SpamModel->check_spam($txtRoomType,true) && !$this->SpamModel->check_spam($txtBudget,true) && !$this->SpamModel->check_spam($txtMessage, FALSE)) {
            $this->db->set('name', $txtName);
            $this->db->set('last_name', $txtLastName);
            $this->db->set('mobile_phone', $txtMobilePhone);
            $this->db->set('email', $txtEmail);
            $this->db->set('room_type', $txtRoomType);
            $this->db->set('budget', $txtBudget);
            $this->db->set('message', $txtMessage);
            $this->db->set('parent_id', $parent_id);
            $this->db->set('enable_status', 'show');
            $this->db->set('sort_priority', '1');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->set('mother_shop_id', $shop_id);
            $this->db->insert('tbl_register_form');
            //select max id
            $this->db->select_max('register_form_id', 'max_id');
            $query = $this->db->get('tbl_register_form');
            $row = $query->row();
            $register_code = $row->max_id + 1100;
            //set update register code
            $data = array(
                'register_code' => $register_code
            );
            $this->db->where('register_form_id', $row->max_id);
            $this->db->update('tbl_register_form', $data);

            $dataEmail = array();
            $dataEmail['txtName'] = $txtName;
            $dataEmail['txtLastName'] = $txtLastName;
            $dataEmail['txtMobilePhone'] = $txtMobilePhone;
            $dataEmail['txtEmail'] = $txtEmail;
            $dataEmail['txtRoomType'] = $txtRoomType;
            $dataEmail['txtBudget'] = $txtBudget;
            $dataEmail['txtMessage'] = $txtMessage;
            $dataEmail['register_code'] = $register_code;
            $dataEmail['shop_id'] = $shop_id;

            ob_start();
            //send to client register
            $register_email_subject = 'Thank you for your interest in ' . $this->ConfigModel->getWebsiteName($shop_id);
            $register_email_message = $this->load->view('email/register_email', $dataEmail, TRUE);
            $this->SendMailModel->smtpmail($txtEmail, $register_email_subject, $register_email_message, $shop_id);

            //send to staff
            $mail_subject = 'Register from ' . $this->ConfigModel->getWebsiteName($shop_id) . ' : (' . $txtEmail . ')';
            $sales_email_message = $this->load->view('email/sales_email', $dataEmail, TRUE);
            $this->SendMailModel->smtpSalesMail($mail_subject, $sales_email_message, $shop_id);

            //send sms to client register
            // $message = "The Stage Condo ขอขอบคุณสำหรับการลงทะเบียน พบกันที่งานพรีเซล 27 ก.ย.นี้ @สนง.ขาย เริ่ม1.89ล. T.0880045551";
            $message = "ห้ามพลาด!! พรีเซล The Stage Condo 27 ก.ย.นี้ เริ่ม1.89ล. T.0880045551";
            $this->SendSMSModel->send($txtMobilePhone, $message);
            ob_end_clean();
            
            redirect('register/thankyou');
        }
    }

    public function thankyou() {
        $this->load->view('thankyou');
    }

    public function testsms($phone = "")
    {
        $message = "Thank you for your registration at TheStageCondo.com. Our Sales Representative will get back to you shortly.";
        $senders = $this->SendSMSModel->send($phone, $message);
        $chk_val = explode('=', $senders);
        $status  = explode(',', $chk_val[1]);
        if ($status == 0) {
            echo 'success : wait to get message on mobile';
        }
    }

}
