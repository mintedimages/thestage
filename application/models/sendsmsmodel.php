<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SendSMSModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function send($telephone = '', $Message = '') {
        $Username  = "MDP-TheStage";
        $Password  = "MDP-TheStage9204";
        $PhoneList = "$telephone";
        //$code = $content_id;
        $Message = iconv('UTF-8', 'TIS-620', $Message);
        $Message = urlencode($Message);
        $Sender = "TheStage";

        $Parameter = "User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";
        $API_URL   = "http://smsmkt.piesoft.net:8999/SMSLink/SendMsg/index.php";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $API_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $Parameter);

        $Result = curl_exec($ch);
        curl_close($ch);
        return $Result;
    }

}