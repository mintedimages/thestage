<?php

class ConfigModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getConfig($config_code = '') {
        $this->db->where('config_code', $config_code);
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    //Frontend Setting
    function getWebsiteName() {
        $this->db->where('config_code', 'website_title');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getLogo() {
        return '';
    }

    function getFavicon() {
        $this->db->where('config_code', 'favicon');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getMetaKeyword() {
        $this->db->where('config_code', 'meta_keywords');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getMetaDescription() {
        $this->db->where('config_code', 'meta_description');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    //Email Setting
    function getSender() {
        $this->db->where('config_code', 'email_account');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getSenderPassword() {
        $this->db->where('config_code', 'password');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getSMTP() {
        $this->db->where('config_code', 'smtp');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getPort() {
        $this->db->where('config_code', 'port');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getReceiver() {
        //return Array
        $this->db->where('config_code', 'to');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        $value = explode(",", $row->config_value);
        return $value;
    }

    function getReceiverCC() {
        //return Array
        $this->db->where('config_code', 'cc');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        $value = explode(",", $row->config_value);
        return $value;
    }

    function getReceiverBCC() {
        //return Array
        $this->db->where('config_code', 'bcc');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        $value = explode(",", $row->config_value);
        return $value;
    }

    //Statistic Setting
    function getGoogleAnalytic() {
        $this->db->where('config_code', 'google_analytic');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getGoogleRemarketing() {
        $this->db->where('config_code', 'google_remarketing');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

    function getFacebookRetarget() {
        $this->db->where('config_code', 'facebook_retarget');
        $query = $this->db->get('mother_config');
        $row = $query->row();
        return $row->config_value;
    }

}

?>
