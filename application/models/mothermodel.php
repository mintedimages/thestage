<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mothermodel
 *
 * @author arsan
 */
class MotherModel extends CI_Model {

    public function getDynamicSingleContent($table_id = 0, $lang_id = 0, $content_id = 0) {
        $tableData = $this->getTableData($table_id);

        $this->db->join("tbl_{$tableData->table_code}_lang", "tbl_{$tableData->table_code}_lang.{$tableData->table_code}_id = tbl_{$tableData->table_code}.{$tableData->table_code}_id");
        $this->db->where("tbl_{$tableData->table_code}_lang.lang_id", $lang_id);
        $this->db->where("tbl_{$tableData->table_code}.enable_status", "show");
        $this->db->where("tbl_{$tableData->table_code}.{$tableData->table_code}_id", $content_id);
        $this->db->order_by("tbl_{$tableData->table_code}.sort_priority");
        $query = $this->db->get("tbl_{$tableData->table_code}");
        return $query->row();
    }

    public function getDynamicContent($table_id = 0, $lang_id = 0, $parent_id = 0) {
        $tableData = $this->getTableData($table_id);

        $this->db->join("tbl_{$tableData->table_code}_lang", "tbl_{$tableData->table_code}_lang.{$tableData->table_code}_id = tbl_{$tableData->table_code}.{$tableData->table_code}_id");
        $this->db->where("tbl_{$tableData->table_code}_lang.lang_id", $lang_id);
        if ($parent_id > 0) {
            $this->db->where("tbl_{$tableData->table_code}.parent_id", $parent_id);
        }
        $this->db->where("tbl_{$tableData->table_code}.enable_status", "show");
        $this->db->order_by("tbl_{$tableData->table_code}.sort_priority");
        return $this->db->get("tbl_{$tableData->table_code}");
    }

    public function getStaticContent($table_id = 0, $lang_id = 0) {
        $tableData = $this->getTableData($table_id);

        $this->db->join("tbl_{$tableData->table_code}_lang", "tbl_{$tableData->table_code}_lang.{$tableData->table_code}_id = tbl_{$tableData->table_code}.{$tableData->table_code}_id");
        $this->db->where("tbl_{$tableData->table_code}_lang.lang_id", $lang_id);
        $query = $this->db->get("tbl_{$tableData->table_code}");
        return $query->row();
    }

    private function getTableData($table_id = 0) {
        $this->db->where('table_id', $table_id);
        $query = $this->db->get('mother_table');
        return $query->row();
    }

    public function getLangData($lang_code = "") {
        $this->db->where('lang_code', $lang_code);
        $query = $this->db->get('mother_lang');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            $this->db->order_by('sort_priority');
            $this->db->limit(1);
            $query = $this->db->get('mother_lang');
            return $query->row();
        }
    }
    
    public function getDefaultContentId($table_id = 0){
        $tableData = $this->getTableData($table_id);
        $table_code = $tableData->table_code;
        $primary = $table_code.'_id';
        $this->db->select("{$tableData->table_code}_id");
        $this->db->where("tbl_{$tableData->table_code}.enable_status", "show");
        $this->db->order_by("tbl_{$tableData->table_code}.sort_priority");
        $this->db->limit(1);
        $query = $this->db->get("tbl_{$tableData->table_code}");
        $row = $query->row();
        return $row->$primary;
    }
}